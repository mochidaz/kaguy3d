use three_d::*;

fn main() {
    let args: Vec<String> = std::env::args().collect();

    let window = Window::new(WindowSettings {
        title: "Kaguy Box".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    })
    .unwrap();
    let context = window.gl().unwrap();

    // Renderer
    let pipeline = ForwardPipeline::new(&context).unwrap();
    let mut camera = Camera::new_perspective(
        &context,
        window.viewport().unwrap(),
        vec3(4.0, 1.5, 4.0),
        vec3(0.0, 1.0, 0.0),
        vec3(0.0, 1.0, 0.0),
        degrees(45.0),
        0.1,
        1000.0,
    )
    .unwrap();
    let mut control = OrbitControl::new(*camera.target(), 1.0, 100.0);

    Loader::load(
        &[
            "assets/kaguya.png",
            "assets/skybox_evening/kaguya.jpg",
            "assets/skybox_evening/kaguya.jpg",
            "assets/skybox_evening/kaguya.jpg",
            "assets/skybox_evening/kaguya.jpg",
            "assets/skybox_evening/kaguya.jpg",
        ],
        move |mut loaded| {
            let box_texture = Texture2D::new(
                &context,
                &loaded.image("assets/kaguya.png").unwrap(),
            )
            .unwrap();
            let box_material = Material {
                albedo_texture: Some(std::rc::Rc::new(box_texture)),
                ..Default::default()
            };
            let mut box_mesh = Model::new(&context, &CPUMesh::cube()).unwrap();
            box_mesh.cull = Cull::Back;

            let skybox = Skybox::new(
                &context,
                &mut loaded
                    .cube_image(
                        "assets/skybox_evening/kaguya.jpg",
                        "assets/skybox_evening/kaguya.jpg",
                        "assets/skybox_evening/kaguya.jpg",
                        "assets/skybox_evening/kaguya.jpg",
                        "assets/skybox_evening/kaguya.jpg",
                        "assets/skybox_evening/kaguya.jpg",
                    )
                    .unwrap(),
            )
            .unwrap();

            let ambient_light = AmbientLight {
                intensity: 0.4,
                color: Color::WHITE,
            };
            let directional_light =
                DirectionalLight::new(&context, 2.0, Color::WHITE, &vec3(0.0, -1.0, -1.0)).unwrap();

            let axes = Axes::new(&context, 0.1, 3.0).unwrap();
            // main loop
            window
                .render_loop(move |mut frame_input| {
                    let mut redraw = frame_input.first_frame;
                    redraw |= camera.set_viewport(frame_input.viewport).unwrap();
                    redraw |= control
                        .handle_events(&mut camera, &mut frame_input.events)
                        .unwrap();

                    // draw
                    if redraw {
                        Screen::write(&context, ClearState::default(), || {
                            pipeline.light_pass(
                                &camera,
                                &[(&box_mesh, &box_material)],
                                Some(&ambient_light),
                                &[&directional_light],
                                &[],
                                &[],
                            )?;
                            skybox.render(&camera)?;
                            Ok(())
                        })
                        .unwrap();
                    }

                    if args.len() > 1 {
                        // To automatically generate screenshots of the examples, can safely be ignored.
                        FrameOutput {
                            screenshot: Some(args[1].clone().into()),
                            exit: true,
                            ..Default::default()
                        }
                    } else {
                        FrameOutput {
                            swap_buffers: redraw,
                            wait_next_event: true,
                            ..Default::default()
                        }
                    }
                })
                .unwrap();
        },
    );
}
